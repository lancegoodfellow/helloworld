package com.sandbox.helloworld;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig {
    @Bean
    public User makeUser(){
        return new User("Lance","Goodfellow");
    }

    @Bean(name="test")
    @Scope("prototype")
    public String addMessage(String name){
        return "asd      "+name;
    }

//    @Bean
//    //@Scope("prototype")
//    public String addMessage(){
//        return "TETAWERT";
//    }
}
