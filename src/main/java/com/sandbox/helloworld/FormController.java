package com.sandbox.helloworld;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FormController {

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public ModelAndView form() {
        return new ModelAndView("form", "user", new User());
    }

    @GetMapping(value = "/addUser")
    public String submit(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, Model model ) {
        User user = new User(firstName, lastName);
        //Do Something
        model.addAttribute("name", user.getName());

        System.out.println(user.getName());

        return "greeting";
    }
}