package com.sandbox.helloworld;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


public class User {
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String firstName;
    private String lastName;

    public User (String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }
    public User(){};

    public String getName(){
        return firstName + " " + lastName;
    }
}
